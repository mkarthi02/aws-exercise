//AWS
region      = "us-east-1"
environment = "production"


vpc_cidr             = "10.0.0.0/16"
public_subnets_cidr  = ["10.0.1.0/24"] //List of Public subnet cidr range
private_subnets_cidr = ["10.0.10.0/24"] //List of private subnet cidr range
availability_zones   = ["us-east-1a","us-east-1b","us-east-1c"] //List of availability zones

public_subnet_ENI_private_ips = ["10.0.1.50"]

