# README #

This README would normally document whatever steps are necessary to get your application up and running.

Deployment/Execution Instructions:

1. Create an EC2 instance in AWS to use as terraform deployment server. (Alternate ways are also available to deploy from CodeBuild, ECS, and EKS Roles based on project/infra requirements)

2. Install Terraform on the EC2 instance

3. Create an IAM role for Terraform & Attached the role to EC2 instance (terraform deployment server)

4. Clone the source code from GIT/Bitbucket to terraform home directory

5. Set the Variables under terraform.tfvars
	//AWS
	region      = "us-east-1"
	environment = "production"
	vpc_cidr             = "10.0.0.0/16"
	public_subnets_cidr  = ["10.0.1.0/24"] //List of Public subnet cidr range
	private_subnets_cidr = ["10.0.10.0/24"] //List of private subnet cidr range
	availability_zones   = ["us-east-1a"] //List of availability zones
	public_subnet_ENI_private_ips = ["10.0.1.50"] //private IP to associate with public IP for webserver

6. Commands to deploy/execute
	terraform init
	terraform plan
	terraform apply --auto-approve
	terraform destroy -> removes all deployed services

7. The requested AWS services will be created & the public IP can be noted from console output
	server_public_ip = "X.X.X.X"

8. The webserver helloworld application running on ec2 web server can be accessed through public IP with port 80
	The HTML page should display as "Karthik webserver for Hellp world"

9.Components:

	├── README.txt
	
	├── main.tf -> contains the core module with all the terraform setup configured
	
	├── variables.tf > contains the variable declaration for main.tf
	
	├── outputs.tf -> provides the console output after the deployment for important data like public IP, endpoint, etc..
	
	├── provider.tf -> defines the provideer & currently it is setup for AWS
	
	├── terraform.tfvars -> contains the actual parameters to be passed for deployment
	
	

10.Terraform setup document:
main.tf contains the core module & does the following:

create a VPC

create a public subnet & private subnet

Create an internet gateway for public subnet and attach to VPC

Create Elastic IP for NAT

Create a NAT instance for private subnet resources to access through NAT

Create  Routing table for private subnet 

Create  Routing table for public subnet

setup Route table associations with route table

setup Default security group for VPC with port 443, 80 & 22(SSH) for inbound access from internet.

Created a EC2 instance in public submit to act as web server

Create a network interface with an ip in the subnet

Create elastic IP to the network interface

Create Ubuntu server and install apache2 for web server

The webserver should display the message in port 80 of its public IP








